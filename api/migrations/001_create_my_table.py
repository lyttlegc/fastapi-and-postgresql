steps = [
    [
        ## Create the table
        """
        CREATE TABLE trades (
            id SERIAL PRIMARY KEY NOT NULL,
            ticker VARCHAR(20) NOT NULL,
            details TEXT NOT NULL,
            entry_date DATE NOT NULL,
            exit_date DATE NOT NULL,
            quantity SMALLINT NOT NULL,
            entry_price MONEY NOT NULL,
            exit_price MONEY NOT NULL,
            p_and_l MONEY NOT NULL
        );
        """,
        ## Drop the table
        """
        DROP TABLE trades;
        """
    ]
]
